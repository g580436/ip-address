import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPAddress {
    public String getIP4Address(){
        String ip = null;
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            ip = localhost.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return ip;
    }
}
